package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Controller {

    public TextField col5Textfield, col4Textfield, col3Textfield, col2Textfield, col1Textfield, tableNameTextfield;
    public Label col5Label, col4Label, col3Label, col2Label, col1Label, currentIDLabel, actionLabel;
    public Button previousButton, previous5Button, nextButton, next5Button, addToDbButton;
    public int currentID = 1;

    Conn conn = new Conn();
    Connection connection = conn.getConnection();
    String tableName = "";

    public void setBlank() throws Exception {
        // hardkodirano da ih je 4, mozda nekako preko getRowNumber
        col1Textfield.setText("");
        col2Textfield.setText("");
        col3Textfield.setText("");
        col4Textfield.setText("");
    }

    public void getDBText(int id, String tableName) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + tableName + " WHERE id=" + id + ";");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            col1Textfield.setText(resultSet.getString(1));
            col2Textfield.setText(resultSet.getString(2));
            col3Textfield.setText(resultSet.getString(3));
            col4Textfield.setText(resultSet.getString(4));
        }
    }

    public void buttonDisable(String tableName) throws Exception {
        addToDbButton.setDisable(true);
        if (currentID <= 5) {
            previous5Button.setDisable(true);
        } else {
            previous5Button.setDisable(false);
        }
        if (currentID <= 1) {
            previousButton.setDisable(true);
        } else {
            previousButton.setDisable(false);
        }
        if (getLastID(tableName) - currentID < 5) {
            next5Button.setDisable(true);
        } else {
            next5Button.setDisable(false);
        }
        if (getLastID(tableName) - currentID < 1) {
            nextButton.setDisable(true);
        } else {
            nextButton.setDisable(false);
        }
    }

    public int getLastID(String tableName) throws Exception {
        String sql = "SELECT MAX(id) FROM " + tableName + ";";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        int lastID = 0;
        while (resultSet.next()) {
            lastID = Integer.parseInt(resultSet.getString(1));
        }
        return lastID;
    }

    public int getNumberOfRows(String tableName) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT COLUMN_NAME FROM information_schema.columns where TABLE_SCHEMA like 'website' AND TABLE_NAME like ?;");
        preparedStatement.setString(1, tableName);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.last();
        int numberOfRows = resultSet.getRow();
        return numberOfRows;
    }

    public List<String> getRows(String tableName) throws Exception {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT COLUMN_NAME FROM information_schema.columns where TABLE_SCHEMA like 'website' AND TABLE_NAME like ?;");
        preparedStatement.setString(1, tableName);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<String> rowList = new ArrayList<>();
        while (resultSet.next()) {
            rowList.add(resultSet.getString(1));
        }
        return rowList;
    }

    public void read(ActionEvent actionEvent) throws Exception {
        String tableName = tableNameTextfield.getText();
        currentID = 1;
        buttonDisable(tableName);
        setBlank();
        getLastID(tableName);

        List<String> rowList = getRows(tableName);

        col1Label.setText(rowList.get(0));
        col2Label.setText(rowList.get(1));
        col3Label.setText(rowList.get(2));
        col4Label.setText(rowList.get(3));

        // hardkodirano da se gleda po ID-u
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + tableName + " WHERE id=" + currentID + ";");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            col1Textfield.setText(rs.getString(1));
            col2Textfield.setText(rs.getString(2));
            col3Textfield.setText(rs.getString(3));
            col4Textfield.setText(rs.getString(4));
        }
        currentIDLabel.setText(col1Textfield.getText() + "/" + getLastID(tableName));
        actionLabel.setText("Tablica " + tableName + " učitana.");
    }

    public void addToDB(ActionEvent actionEvent) throws Exception {
        String tableName = tableNameTextfield.getText();
        int id = getLastID(tableName);
        System.out.println(id);
        id++;
        System.out.println(id);
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO " + tableName + " VALUES (?, ?, ?, ?);");
        preparedStatement.setInt(1, id);
        preparedStatement.setString(2, col2Textfield.getText());
        preparedStatement.setString(3, col3Textfield.getText());
        preparedStatement.setString(4, col4Textfield.getText());
        preparedStatement.executeUpdate();
        actionLabel.setText("Novi ID dodan");
        col1Textfield.setDisable(false);
        addToDbButton.setDisable(true);
    }

    public void add(ActionEvent actionEvent) throws Exception {
        String tableName = tableNameTextfield.getText();
        setBlank();
        int lastID = getLastID(tableName);
        lastID++;
        col1Textfield.setText(Integer.toString(lastID));
        col1Textfield.setDisable(true);
        addToDbButton.setDisable(false);
    }

    public void update(ActionEvent actionEvent) throws Exception {
        String tableName = tableNameTextfield.getText();
        /*
        int rowNumber = getNumberOfRows();
        List<String> rowName = getRows();
        */
        String sql = "UPDATE " + tableName + " SET " + col2Label.getText() + " = '" + col2Textfield.getText() + "', " + col3Label.getText() + " = '" + col3Textfield.getText() +
                "', " + col4Label.getText() + " = '" + col4Textfield.getText() + "' WHERE id= " + currentID + ";";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.executeUpdate();
        actionLabel.setText("Promjena na IDu " + currentID);
    }

    public void delete(ActionEvent actionEvent) throws Exception {
        String tableName = tableNameTextfield.getText();
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM " + tableName + " WHERE id=" + currentID + ";");
        preparedStatement.executeUpdate();
        setBlank();
        actionLabel.setText("ID " + currentID + " obrisan");
    }

    public void previous(ActionEvent actionEvent) throws Exception {
        setBlank();
        String tableName = tableNameTextfield.getText();
        currentID--;
        getDBText(currentID, tableName);
        currentIDLabel.setText(currentID + "/" + getLastID(tableName));
        buttonDisable(tableName);
    }

    public void previous5(ActionEvent actionEvent) throws Exception {
        setBlank();
        String tableName = tableNameTextfield.getText();
        currentID -= 5;
        getDBText(currentID, tableName);
        currentIDLabel.setText(currentID + "/" + getLastID(tableName));
        buttonDisable(tableName);
    }

    public void next5(ActionEvent actionEvent) throws Exception {
        setBlank();
        String tableName = tableNameTextfield.getText();
        currentID += 5;
        getDBText(currentID, tableName);
        currentIDLabel.setText(currentID + "/" + getLastID(tableName));
        buttonDisable(tableName);
    }

    public void next(ActionEvent actionEvent) throws Exception {
        setBlank();
        String tableName = tableNameTextfield.getText();
        currentID++;
        getDBText(currentID, tableName);
        currentIDLabel.setText(currentID + "/" + getLastID(tableName));
        buttonDisable(tableName);
    }
}