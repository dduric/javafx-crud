package sample;

import java.sql.Connection;
import java.sql.DriverManager;


public class Conn {

    public Connection connection;

    public Connection getConnection()
    {
        String userName="root";
        String password="";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/website?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=CET", userName, password);

            return connection;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}